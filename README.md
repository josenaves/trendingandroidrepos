# Trending Android Repos #

This is an Android Native app project that lists the trending Android repositories on Github.

It has 2 screens:
* A list of trending Github repositories of android
* A simple “Detail View” of the detail information of the repository, been accessed when the user taps on one of the repositories of the list.

### Libraries used in this project

* [Retrofit2](http://square.github.io/retrofit/)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* [Room](https://developer.android.com/topic/libraries/architecture/room.html)
