package com.josenaves.androidrepos.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.josenaves.androidrepos.data.network.GithubService;
import com.josenaves.androidrepos.data.db.Repo;
import com.josenaves.androidrepos.data.db.GithubDao;
import com.josenaves.androidrepos.model.Item;
import com.josenaves.androidrepos.model.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GithubRepository {

    private final static String TAG = GithubRepository.class.getSimpleName();

    private final GithubService githubWebservice;
    private final GithubDao githubDao;
    private final Executor executor;

    public GithubRepository(GithubService githubWebservice, GithubDao githubDao, Executor executor) {
        this.githubWebservice = githubWebservice;
        this.githubDao = githubDao;
        this.executor = executor;
    }

    public LiveData<List<Repo>> getTrendingRepositories() {
        // This is not an optimal implementation, we'll fix it below
        final MutableLiveData<List<Repo>> data = new MutableLiveData<>();

        githubWebservice.getTrendingRepos().enqueue(new Callback<Repositories>() {
            @Override
            public void onResponse(Call<Repositories> call, Response<Repositories> response) {
                Log.i(TAG, "WS returned: " + response.body());

                // convert Repositories to List<Repo>
                data.setValue(toListRepo(response.body()));
            }

            @Override
            public void onFailure(Call<Repositories> call, Throwable t) {
                Log.w(TAG, "Failure calling ws: " + t);
            }
        });
        return data;
    }

    private List<Repo> toListRepo(Repositories repoList){
        List<Repo> list = new ArrayList<>();
        int total = repoList.getTotalCount();

        if (total > 0) {
            int max = total > 10 ? 10 : total;
            // get first 10 repos
            for (int i = 0; i < max; i++) {
                Item item = repoList.getItems().get(i);
                Repo repo = new Repo();
                repo.setDescription(item.getDescription());
                repo.setName(item.getName());
                repo.setId(repo.getId());
                list.add(repo);
            }
        }
        return list;
    }
}
