package com.josenaves.androidrepos.data.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface GithubDao {
    @Insert(onConflict = REPLACE)
    void save(Repo repo);

    @Query("SELECT * from repo")
    LiveData<Repo> load();
}
