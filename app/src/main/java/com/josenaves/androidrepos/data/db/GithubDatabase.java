package com.josenaves.androidrepos.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Repo.class}, version = 1)
public abstract class GithubDatabase extends RoomDatabase {
    public abstract GithubDao githubDao();
}
