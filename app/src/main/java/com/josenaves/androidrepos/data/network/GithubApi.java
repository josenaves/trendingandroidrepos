package com.josenaves.androidrepos.data.network;

import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class GithubApi {
    private static final String BASE_URL = "https://api.github.com/";

    private static Retrofit retrofit;
    private static GithubService repoService;

    private GithubApi() {}

    public static GithubService getInstance() {
        if (repoService != null) {
            return repoService;
        }

        if (retrofit == null) {
            initializeRetrofit();
        }

        repoService = retrofit.create(GithubService.class);
        return repoService;
    }

    private static void initializeRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
    }
}
