package com.josenaves.androidrepos.data.network;

import com.josenaves.androidrepos.model.Repositories;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GithubService {
    // https://api.github.com/search/repositories?q=topic:android
    @GET("/search/repositories?q=topic:android")
    Call<Repositories> getTrendingRepos();
}
