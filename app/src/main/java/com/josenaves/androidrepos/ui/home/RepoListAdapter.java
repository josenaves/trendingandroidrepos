package com.josenaves.androidrepos.ui.home;

import android.arch.lifecycle.LifecycleOwner;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.josenaves.androidrepos.R;
import com.josenaves.androidrepos.model.Item;

import java.util.ArrayList;
import java.util.List;

public class RepoListAdapter extends RecyclerView.Adapter <RepoListAdapter.RepoViewHolder> {

    private final List<Item> data = new ArrayList<>();

    RepoListAdapter(RepoViewModel viewModel, LifecycleOwner lifecycleOwner) {
        viewModel.getRepositories().observe(lifecycleOwner, repos -> {
            data.clear();
            if (repos != null) {
                data.addAll(repos.getItems());
            }

            notifyDataSetChanged();  // TODO: Use DiffUtil when we have AutoValue models
        });
        setHasStableIds(true);
    }

    @Override
    public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.view_repo_list_item, parent, false);

        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepoViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    static final class RepoViewHolder extends RecyclerView.ViewHolder {
        private TextView repoNameTextView;
        private TextView repoDescriptionTextView;
        private TextView forksTextView;
        private TextView starsTextView;

        RepoViewHolder(View itemView) {
            super(itemView);
            repoNameTextView = itemView.findViewById(R.id.tv_repo_name);
            repoDescriptionTextView = itemView.findViewById(R.id.tv_repo_description);
            forksTextView = itemView.findViewById(R.id.tv_forks);
            starsTextView = itemView.findViewById(R.id.tv_stars);
        }

        void bind(Item repoItem) {
            repoNameTextView.setText(repoItem.getName());
            repoDescriptionTextView.setText(repoItem.getDescription());
            forksTextView.setText(String.valueOf(repoItem.getForks()));
            starsTextView.setText(String.valueOf(repoItem.getStargazersCount()));
        }
    }
}
