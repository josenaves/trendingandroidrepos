package com.josenaves.androidrepos.ui.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.josenaves.androidrepos.data.GithubRepository;
import com.josenaves.androidrepos.data.network.GithubApi;
import com.josenaves.androidrepos.model.Repositories;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoViewModel extends ViewModel {

    private static final String TAG = RepoViewModel.class.getSimpleName();

    private final MutableLiveData<Repositories> repositories = new MutableLiveData<>();
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    private Call<Repositories> repoCall;

    private GithubRepository githubRepository;

    public RepoViewModel() {
        fetchRepos();
    }

    private void fetchRepos() {
        loading.setValue(true);
        repoCall = GithubApi.getInstance().getTrendingRepos();
        repoCall.enqueue(new Callback<Repositories>() {
            @Override
            public void onResponse(Call<Repositories> call, Response<Repositories> response) {
                repoLoadError.setValue(false);
                repositories.setValue(response.body());
                loading.setValue(false);
                repoCall = null;
            }

            @Override
            public void onFailure(Call<Repositories> call, Throwable t) {
                Log.e(TAG, "Error loading repos", t);
                repoLoadError.setValue(true);
                loading.setValue(false);
                repoCall = null;
            }
        });
    }

    @Override
    protected void onCleared() {
        if (repoCall != null) {
            repoCall.cancel();
            repoCall = null;
        }
    }

    LiveData<Repositories> getRepositories() {
        return repositories;
    }

    LiveData<Boolean> getRepoLoadError() {
        return repoLoadError;
    }

    LiveData<Boolean> getLoading() {
        return repoLoadError;
    }

//    LiveData<Repositories> getRepos() {
//        return repos;
////        if (repos == null) {
////            repos = new MutableLiveData<>();
////            loadRepos();
////        }
////        return repos;
//    }
//
//
//    private void loadRepos() {
//        // do an asynchronous operation to fetch repos
//        new LoadRepoTask(githubRepository).execute();
//    }
//
//    private class LoadRepoTask extends AsyncTask<Void, Void, Void> {
//        private GithubRepository repository;
//
//        LoadRepoTask(GithubRepository repo) {
//            this.repository = repo;
//        }
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            // make the call to webservice
//            repository.getTrendingRepositories();
//            return null;
//        }
//    }
}
