package com.josenaves.androidrepos.util;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.josenaves.androidrepos.AppExecutors;
import com.josenaves.androidrepos.data.GithubRepository;
import com.josenaves.androidrepos.data.db.GithubDao;
import com.josenaves.androidrepos.data.db.GithubDatabase;
import com.josenaves.androidrepos.data.network.GithubService;

import java.util.concurrent.Executor;

import retrofit2.Retrofit;

public class InjectorUtils {

    public static final String BASE_URL = "https://api.github.com/";

//    public static GithubRepository provideGithubRepository(Application app, Executor executor) {
//        return new GithubRepository(
//                provideGithubService(),
//                provideRepoDao(provideDatabase(app)),
//                AppExecutors.getInstance().diskIO()
//        );
//    }

    public static GithubDatabase provideDatabase(Application app) {
        return Room.databaseBuilder(app, GithubDatabase.class, "github.db").build();
    }

    public static GithubDao provideRepoDao(GithubDatabase database) {
        return database.githubDao();
    }
//
//    private static GithubService provideGithubService() {
//        return new Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
//                .build()
//                .create(GithubService.class);
//    }
}
